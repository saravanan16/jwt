const jwt = require("jsonwebtoken");

function login(req, res) {
  let token = jwt.sign(
    {
      userName: req.body.userName,
    },
    "secret",
    { expiresIn: "60s" }
  );
  res.json({ token: token });
}
async function user(req, res) {
  let token = req.headers.authorization;
  let decoded = jwt.verify(token, "secret");
  console.log(decoded);
}
module.exports = {
  login: login,
  user: user,
};
